/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_seetaface_v6_FaceDetector */

#ifndef _Included_com_seetaface_v6_FaceDetector
#define _Included_com_seetaface_v6_FaceDetector
#ifdef __cplusplus
extern "C" {
#endif
#undef com_seetaface_v6_FaceDetector_PROPERTY_MIN_FACE_SIZE
#define com_seetaface_v6_FaceDetector_PROPERTY_MIN_FACE_SIZE 0L
#undef com_seetaface_v6_FaceDetector_PROPERTY_THRESHOLD
#define com_seetaface_v6_FaceDetector_PROPERTY_THRESHOLD 1L
#undef com_seetaface_v6_FaceDetector_PROPERTY_MAX_IMAGE_WIDTH
#define com_seetaface_v6_FaceDetector_PROPERTY_MAX_IMAGE_WIDTH 2L
#undef com_seetaface_v6_FaceDetector_PROPERTY_MAX_IMAGE_HEIGHT
#define com_seetaface_v6_FaceDetector_PROPERTY_MAX_IMAGE_HEIGHT 3L
#undef com_seetaface_v6_FaceDetector_PROPERTY_NUMBER_THREADS
#define com_seetaface_v6_FaceDetector_PROPERTY_NUMBER_THREADS 4L
/*
 * Class:     com_seetaface_v6_FaceDetector
 * Method:    nativeCreateEngine
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceDetector_nativeCreateEngine
  (JNIEnv *, jobject, jstring);

/*
 * Class:     com_seetaface_v6_FaceDetector
 * Method:    nativeDestroyEngine
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_FaceDetector_nativeDestroyEngine
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_FaceDetector
 * Method:    nativeSetProperty
 * Signature: (ID)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_FaceDetector_nativeSetProperty
  (JNIEnv *, jobject, jint, jdouble);

/*
 * Class:     com_seetaface_v6_FaceDetector
 * Method:    nativeGetProperty
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_com_seetaface_v6_FaceDetector_nativeGetProperty
  (JNIEnv *, jobject, jint);

/*
 * Class:     com_seetaface_v6_FaceDetector
 * Method:    nativeDetectFaces
 * Signature: (Lcom/seetaface/v6/SeetaImageData;)[Lcom/seetaface/v6/SeetaFaceInfo;
 */
JNIEXPORT jobjectArray JNICALL Java_com_seetaface_v6_FaceDetector_nativeDetectFaces
  (JNIEnv *, jobject, jobject);

#ifdef __cplusplus
}
#endif
#endif
