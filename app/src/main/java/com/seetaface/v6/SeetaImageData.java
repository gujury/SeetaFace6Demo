package com.seetaface.v6;

public class SeetaImageData {
    public int width;
    public int height;
    // 3 channels bgr color buffer or 1 gray buffer
    public int channels;
    // image data, bgr format
    public byte[] data;
}
