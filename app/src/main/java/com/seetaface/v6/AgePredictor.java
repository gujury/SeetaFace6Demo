package com.seetaface.v6;

import android.util.Log;

public class AgePredictor {

    private static final String TAG = "AgePredictor";

    public static final int PROPERTY_NUMBER_THREADS = 4;
    public static final int PROPERTY_ARM_CPU_MODE = 5;

    public static final String MODEL_AGE_PREDICTOR = "age_predictor.csta";

    static {
        System.loadLibrary("AgePredictor");
    }

    public AgePredictor() {
        String agePredictorModel = SeetaUtils.getSeetaFaceModelPath(MODEL_AGE_PREDICTOR);
        int result = nativeCreateEngine(agePredictorModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);

        nativeSetProperty(PROPERTY_NUMBER_THREADS, 1);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String agePredictorModel);
    private native int nativeDestroyEngine();

    private native int nativeGetCropFaceWidth();
    private native int nativeGetCropFaceHeight();
    private native int nativeGetCropFaceChannels();
    private native boolean nativeCropFace(SeetaImageData image, SeetaPointF[] pointFs, SeetaImageData crop);

    private native int nativePredictAge(SeetaImageData image);
    private native int nativePredictAgeWithCrop(SeetaImageData image, SeetaPointF[] pointFs);

    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
}
