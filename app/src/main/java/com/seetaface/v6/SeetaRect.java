package com.seetaface.v6;

public class SeetaRect {
    public int x;
    public int y;
    public int width;
    public int height;

    @Override
    public String toString() {
        return "SeetaRect{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
