package com.seetaface.v6;

import android.util.Log;

public class FaceDetector {
    private static final String TAG = "FaceDetector";

    public static final int PROPERTY_MIN_FACE_SIZE = 0; // 最小人脸属性
    public static final int PROPERTY_THRESHOLD = 1; // 检测器阈值属性
    public static final int PROPERTY_MAX_IMAGE_WIDTH = 2; // 支持输入的图像的最大宽度
    public static final int PROPERTY_MAX_IMAGE_HEIGHT = 3; // 支持输入的图像的最大高度
    public static final int PROPERTY_NUMBER_THREADS = 4; // 计算线程数属性

    public static final String DETECTOR_MODEL = "face_detector.csta";

    static {
        System.loadLibrary("FaceDetector");
    }

    public FaceDetector() {
        String detectorModel = SeetaUtils.getSeetaFaceModelPath(DETECTOR_MODEL);
        int result = nativeCreateEngine(detectorModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);

        // set default property
        setDetectorProperty(PROPERTY_MIN_FACE_SIZE, 50);
        setDetectorProperty(PROPERTY_THRESHOLD, 0.9f);
        setDetectorProperty(PROPERTY_NUMBER_THREADS, 1);

        Log.d(TAG, "-------gu--- default_max_width:" + getDetectorProperty(PROPERTY_MAX_IMAGE_WIDTH));
        Log.d(TAG, "-------gu--- default_max_height:" + getDetectorProperty(PROPERTY_MAX_IMAGE_HEIGHT));
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    public void setDetectorProperty(int property, double value) {
        nativeSetProperty(property, value);
    }

    public double getDetectorProperty(int type) {
        return nativeGetProperty(type);
    }

    public SeetaFaceInfo[] detectFaces(SeetaImageData image) {
        return nativeDetectFaces(image);
    }

    private native int nativeCreateEngine(String detectorModel);
    private native int nativeDestroyEngine();
    private native void nativeSetProperty(int type, double value);
    private native double nativeGetProperty(int type);
    private native SeetaFaceInfo[] nativeDetectFaces(SeetaImageData image);
}
