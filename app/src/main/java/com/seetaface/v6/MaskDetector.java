package com.seetaface.v6;

import android.util.Log;

public class MaskDetector {

    private static final String TAG = "MaskDetector";

    public static final String MASK_DETECTOR_MODEL = "mask_detector.csta";

    static {
        System.loadLibrary("MaskDetector");
    }

    public MaskDetector() {
        String maskDetectorModel = SeetaUtils.getSeetaFaceModelPath(MASK_DETECTOR_MODEL);
        int result = nativeCreateEngine(maskDetectorModel);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String detectorModel);
    private native int nativeDestroyEngine();
    private native boolean nativeDetectMask(SeetaImageData imageData, SeetaRect faceRect, float[] scores);
}
