/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_seetaface_v6_QualityChecker */

#ifndef _Included_com_seetaface_v6_QualityChecker
#define _Included_com_seetaface_v6_QualityChecker
#ifdef __cplusplus
extern "C" {
#endif
#undef com_seetaface_v6_QualityChecker_QUALITY_BRIGHTNESS
#define com_seetaface_v6_QualityChecker_QUALITY_BRIGHTNESS 0L
#undef com_seetaface_v6_QualityChecker_QUALITY_CLARITY
#define com_seetaface_v6_QualityChecker_QUALITY_CLARITY 1L
#undef com_seetaface_v6_QualityChecker_QUALITY_LBN
#define com_seetaface_v6_QualityChecker_QUALITY_LBN 2L
#undef com_seetaface_v6_QualityChecker_QUALITY_POSE
#define com_seetaface_v6_QualityChecker_QUALITY_POSE 3L
#undef com_seetaface_v6_QualityChecker_QUALITY_POSE_EX
#define com_seetaface_v6_QualityChecker_QUALITY_POSE_EX 4L
#undef com_seetaface_v6_QualityChecker_QUALITY_RESOLUTION
#define com_seetaface_v6_QualityChecker_QUALITY_RESOLUTION 5L
#undef com_seetaface_v6_QualityChecker_LBN_PROPERTY_NUMBER_THREADS
#define com_seetaface_v6_QualityChecker_LBN_PROPERTY_NUMBER_THREADS 4L
#undef com_seetaface_v6_QualityChecker_LBN_PROPERTY_ARM_CPU_MODE
#define com_seetaface_v6_QualityChecker_LBN_PROPERTY_ARM_CPU_MODE 5L
#undef com_seetaface_v6_QualityChecker_LBN_PROPERTY_LIGHT_THRESH
#define com_seetaface_v6_QualityChecker_LBN_PROPERTY_LIGHT_THRESH 10L
#undef com_seetaface_v6_QualityChecker_LBN_PROPERTY_BLUR_THRESH
#define com_seetaface_v6_QualityChecker_LBN_PROPERTY_BLUR_THRESH 11L
#undef com_seetaface_v6_QualityChecker_LBN_PROPERTY_NOISE_THRESH
#define com_seetaface_v6_QualityChecker_LBN_PROPERTY_NOISE_THRESH 12L
#undef com_seetaface_v6_QualityChecker_LBN_LIGHT_BRIGHT
#define com_seetaface_v6_QualityChecker_LBN_LIGHT_BRIGHT 0L
#undef com_seetaface_v6_QualityChecker_LBN_LIGHT_DARK
#define com_seetaface_v6_QualityChecker_LBN_LIGHT_DARK 1L
#undef com_seetaface_v6_QualityChecker_LBN_BLUR_CLEAR
#define com_seetaface_v6_QualityChecker_LBN_BLUR_CLEAR 0L
#undef com_seetaface_v6_QualityChecker_LBN_BLUR_BLUR
#define com_seetaface_v6_QualityChecker_LBN_BLUR_BLUR 1L
#undef com_seetaface_v6_QualityChecker_LBN_NOISE_HAVE
#define com_seetaface_v6_QualityChecker_LBN_NOISE_HAVE 0L
#undef com_seetaface_v6_QualityChecker_LBN_NOISE_NO
#define com_seetaface_v6_QualityChecker_LBN_NOISE_NO 1L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_YAW_LOW_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_YAW_LOW_THRESHOLD 0L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_YAW_HIGH_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_YAW_HIGH_THRESHOLD 1L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_PITCH_LOW_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_PITCH_LOW_THRESHOLD 2L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_PITCH_HIGH_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_PITCH_HIGH_THRESHOLD 3L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_ROLL_LOW_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_ROLL_LOW_THRESHOLD 4L
#undef com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_ROLL_HIGH_THRESHOLD
#define com_seetaface_v6_QualityChecker_POSE_EX_PROPERTY_ROLL_HIGH_THRESHOLD 5L
/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartBrightnessChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartBrightnessChecker__
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartBrightnessChecker
 * Signature: (FFFF)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartBrightnessChecker__FFFF
  (JNIEnv *, jobject, jfloat, jfloat, jfloat, jfloat);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeBrightnessCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativeBrightnessCheck
  (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopBrightnessChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopBrightnessChecker
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartClarityChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartClarityChecker__
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartClarityChecker
 * Signature: (FF)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartClarityChecker__FF
  (JNIEnv *, jobject, jfloat, jfloat);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeClarityCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativeClarityCheck
  (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopClarityChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopClarityChecker
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartLBNChecker
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartLBNChecker
  (JNIEnv *, jobject, jstring);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeLBNDetect
 * Signature: (Lcom/seetaface/v6/SeetaImageData;[Lcom/seetaface/v6/SeetaPointF;[I[I[I)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_QualityChecker_nativeLBNDetect
  (JNIEnv *, jobject, jobject, jobjectArray, jintArray, jintArray, jintArray);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeLBNSetProperty
 * Signature: (ID)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_QualityChecker_nativeLBNSetProperty
  (JNIEnv *, jobject, jint, jdouble);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeLBNGetProperty
 * Signature: (I)D
 */
JNIEXPORT jdouble JNICALL Java_com_seetaface_v6_QualityChecker_nativeLBNGetProperty
  (JNIEnv *, jobject, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopLBNChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopLBNChecker
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativePoseCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativePoseCheck
  (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartPoseExChecker
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartPoseExChecker
  (JNIEnv *, jobject, jstring);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativePoseExCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativePoseExCheck
  (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativePoseExSetProperty
 * Signature: (IF)V
 */
JNIEXPORT void JNICALL Java_com_seetaface_v6_QualityChecker_nativePoseExSetProperty
  (JNIEnv *, jobject, jint, jfloat);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativePoseExGetProperty
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_com_seetaface_v6_QualityChecker_nativePoseExGetProperty
  (JNIEnv *, jobject, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopPoseExChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopPoseExChecker
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartResolutionChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartResolutionChecker__
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartResolutionChecker
 * Signature: (FF)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartResolutionChecker__FF
  (JNIEnv *, jobject, jfloat, jfloat);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeResolutionCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativeResolutionCheck
  (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopResolutionChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopResolutionChecker
  (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartIntegrityChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartIntegrityChecker__
        (JNIEnv *, jobject);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStartIntegrityChecker
 * Signature: (FF)I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStartIntegrityChecker__FF
        (JNIEnv *, jobject, jfloat, jfloat);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeIntegrityCheck
 * Signature: (Lcom/seetaface/v6/SeetaImageData;Lcom/seetaface/v6/SeetaRect;[Lcom/seetaface/v6/SeetaPointF;I)Lcom/seetaface/v6/QualityResult;
 */
JNIEXPORT jobject JNICALL Java_com_seetaface_v6_QualityChecker_nativeIntegrityCheck
        (JNIEnv *, jobject, jobject, jobject, jobjectArray, jint);

/*
 * Class:     com_seetaface_v6_QualityChecker
 * Method:    nativeStopIntegrityChecker
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_seetaface_v6_QualityChecker_nativeStopIntegrityChecker
        (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
