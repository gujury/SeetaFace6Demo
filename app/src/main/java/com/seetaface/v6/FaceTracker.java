package com.seetaface.v6;

import android.util.Log;

public class FaceTracker {

    private static final String TAG = "FaceTracker";

    public static final String MODEL_TRACKER = "face_detector.csta";

    static {
        System.loadLibrary("FaceAntiSpoofing");
    }

    public FaceTracker(int width, int height) {
        String trackerModel = SeetaUtils.getSeetaFaceModelPath(MODEL_TRACKER);
        int result = nativeCreateEngine(trackerModel, width, height);
        Log.d(TAG, "-------gu--- createEngine result:" + result);
    }

    public void destroy() {
        int result = nativeDestroyEngine();
        Log.d(TAG, "-------gu--- destroyEngine result:" + result);
    }

    private native int nativeCreateEngine(String trackerModel, int width, int height);
    private native int nativeDestroyEngine();

    private native void nativeSetSingleCalculationThreads(int num);
    private native void nativeSetMinFaceSize(int size);
    private native int nativeGetMinFaceSize();
    private native void nativeSetThreshold(float thresh);
    private native float nativeGetScoreThreshold();
    private native void nativeSetVideoStable(boolean stable);
    private native boolean nativeGetVideoStable();

    private native SeetaTrackingFaceInfo[] nativeTrack(SeetaImageData image);
    private native SeetaTrackingFaceInfo[] nativeTrack(SeetaImageData image, int frameIndex);
}
